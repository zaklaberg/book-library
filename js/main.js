

class BookLibrary {
    STORE_URL = "http://localhost:3000/books";

    constructor() {
        this.fetchBooks()
            .then(books => { this.showBooks(books); return books; })
            .then(books => { this.books = books; this.displayedBooks = books; })
            .catch(err => console.log(err))

        document.getElementById('addBook_submit').addEventListener('click', this.addBook.bind(this));
        document.getElementById('searchForBook').addEventListener('keyup', this.onBookFilter.bind(this));

        this.form = {
            "elTitle": document.getElementById("addBook_title"),
            "elAuthor": document.getElementById('addBook_author'),
            "elUrlToCover": document.getElementById('addBook_urlToCover')
        }
    }

    onBookFilter(e) {
        const booksToShow = this.books.filter(book => {
            const matchesTitle = book.title.toUpperCase().includes(e.target.value.toUpperCase());
            const matchesAuthor = book.author.toUpperCase().includes(e.target.value.toUpperCase());
            return matchesAuthor || matchesTitle;
        })

        const displayedBookIds = this.displayedBooks.map(book => book.id);
        const booksToShowIds = booksToShow.map(book => book.id);
        if (JSON.stringify(displayedBookIds) === JSON.stringify(booksToShowIds)) return;

        this.showBooks(booksToShow)
    }

    addBook() {
        const formValues = Object.values(this.form).map(elForm => elForm.value);
        if (formValues.some(val => val === '')) {
            alert('Please fill out the form before submitting.')
            return;
        }

        const formData = {
            title: formValues[0],
            author: formValues[1],
            urlToCover: formValues[2]
        }

        fetch(this.STORE_URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(formData)
        })
        .then(resp => resp.json())
        .then(data => console.log(`success: ${data}`))
        .catch(error => console.log(`woopsie: ${error}`))
    }

    fetchBooks() {
        return fetch(this.STORE_URL)
            .then(resp => {
                if (!resp.ok) throw new Error('Bad HTTP response');
                return resp;
            })
            .then(resp => resp.json())
    }

    showBooks(books) {
        const booksHTML = books.sort((a,b) => a.title.localeCompare(b.title)).map(book => {
            return `<li class="book">
                <h2>${book.title}</h2>
                <p>${book.author}</p>
                <img src="${book.urlToCover}" alt="Cover image" />
                <span>
                <label for="${book.id + '_isRead'}">Read</label>
                <input id="${book.id + '_isRead'}" type="checkbox" ${book.read ? 'checked' : ''} />
                </span>
            </li>`
        })

        document.getElementById('books').innerHTML = booksHTML.join('');

        books.map(book => book.id + '_isRead')
            .forEach(bookId => {
                const elBookRead = document.getElementById(bookId)
                elBookRead.addEventListener('click', this.toggleBookRead.bind(this))
        })

        this.displayedBooks = books;
    }

    toggleBookRead(e) {
        const bookId = e.target.id.slice(0, -7);
        const updatedBookData = {
            read: e.target.checked,
            id: bookId
        }
        
        const url = this.STORE_URL + `/${bookId}`
        
        fetch(url, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(updatedBookData)
        })
    }
}

const lib = new BookLibrary();